
import { Container } from 'react-bootstrap';

import './App.css';
import AppNavBar from './components/AppNavBar';
import Courses from './pages/Courses'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';

/*
  All other components/pages will be contained in our main component: <App />
  
  <>..</> - Fragment which ensures that adjacent JSX elements will be rendered and avoid this error.
*/
function App() {
  return (
    <>
      <AppNavBar />
      <Container fluid>
          {/*<Home />
          <Courses />
          <Register />
          */}
          <Login />
      </Container>
    </>
  );
}

export default App;
